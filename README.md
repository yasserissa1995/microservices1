# BackEnd Service
## Topics

- #### Spring Rest APIs
  - ##### GET, POST, PUT, DELETE  and PATCH methods
  - ##### Path variable, request params, request body response body, valid

- #### Spring Data Rest ( without controller and service layer boilerplate code used for lookups data)


- #### Exceptions : @ControllerAdvice and @ExceptionHandler


- #### Scheduling : run Jobs


- #### Profiles :  Work in different environments


- #### Auditing service
  - ##### Spring data envers and hibernate envers
  - ##### Logging Aspect
    - ###### Pointcut and Around : All requests


- #### Caches

- #### Jackson (Serializable , deSerializable and make Rest API support polymorphism)


- #### Startup Application Listener (Allow anything to run after the service up)


- #### Reflections (get meta data for class)


- #### Repository layer
  - ##### Inheritance
    - ###### Base Repository
  - ##### Custom Repositories (EntityManager) (DDL Language )
  - ##### Sort, Paging, limitations


- #### Service layer
  - ##### Transactions
    - ###### readOnly , timeout , rollback
    - ###### isolation
    - ###### propagation
      - ###### REQUIRED
        - ###### REQUIRED is the default , Creates new transaction if no active transaction is found
      - ###### SUPPORTS
        - ###### do not creates new transactions if no active transaction is found
      - ###### MANDATORY
        - ###### if there is an active transaction, then it will be used. If there isn't, then Spring throws an exception
      - ###### NEVER
        - ###### Spring throws an exception if there's an active transaction
      - ###### NOT_SUPPORTED
        - ###### Suspend transactions and execute them as non-transactions
      - ###### REQUIRES_NEW
        - ###### Suspend transactions and cxreate a new
      - ###### NEVER
        - ###### Spring throws an exception if there's an active transaction


- #### Model (Domain)
  - ##### Lombook (boilerplate code)
  - ##### Relationship (Inheritance, Polymorphism, Compositions, Embedded, Secondary Table)
  - ##### Fetching Data (lazy , eager , entity graph and join queries)
  - ##### Data Fileds (primary key , composite primary key , generated value and sequance , version , enums , Transient  )
  - ##### Converters
  - ##### Validations (not null , not blank , not empty , required , size , email pattren)
  - ##### Projections (DTOs Mappers)
  - ##### Searching
    - ###### Hibernate Specification and Hibernate ExampleMatcher




<table style="background: red">
<th>Annotation</th>
<th>Descriptions</th>

<tr>

1111

</tr>

<tr>

1111

</tr>

<tr>

1111

</tr>

<tr>

1111

</tr>

</table>



| Annotation                                                                           | Description |
|--------------------------------------------------------------------------------------| ------ |
| @Entity                                                                              |
| @Table(name = "")                                                                    |
| @Inheritance(strategy = InheritanceType.JOINED)                                      |
| @Id                                                                                  |
| @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator") |
| @SequenceGenerator(name = "sequenceGenerator")                                       |
| @Column(name = "")                                                                   |
| @Version                                                                             |
| @NotNull                                                                             |
| @NotEmpty                                                                            |
| @NotBlank                                                                            |
| @Enumerated(EnumType.STRING)                                                         |
| @Basic(optional = true)                                                              |
| @PrimaryKeyJoinColumn(name = "ID", referencedColumnName = "ID")                      |
| @Audited                                                                             |
| @NotAudited                                                                          |
| @Size(max = 50)                                                                      |
| @Email                                                                               |
| @Formula("(TIMESTAMPDIFF(YEAR,birth_date,CURDATE()))")                               |
| @NotFound(action = NotFoundAction.IGNORE)                                            |
| @ManyToMany                                                                          |
| @OneToMany                                                                           |
| @ManyToOne                                                                           |
| @Where(clause = "active = true")                                                     |
| @Convert(converter = Converter.class)                                                |
| @Transient                                                                           |
| @MappedSuperclass                                                                    |
| @JoinTable                                                                           |
| @JoinColumn                                                                          |
| @CreatedBy                                                                           |
| @CreatedDate                                                                         |
| @LastModifiedBy                                                                      |
| @LastModifiedDate                                                                    |
| @IdClass(AccountPrimary.class)                                                       |
| @EmbeddedId                                                                          |
| @Embeddable                                                                          |
| @Getter                                                                              |
| @Setter                                                                              |
| @AllArgsConstructor                                                                  |
| @NoArgsConstructor                                                                   |
| @ToString                                                                            |
| @EqualsAndHashCode                                                                   |
| @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)                                  |
| @Aspect                                                                              |
| @Autowired                                                                           |
| @Pointcut                                                                            |
| @Around                                                                              |
| @EnableCaching                                                                       |
| @EnableJpaRepositories                                                               |
| @EntityScan                                                                          |
| @EnableJpaAuditing                                                                   |
| @EnableTransactionManagement                                                         |
| @Profile                                                                             |
| @Primary                                                                             |
| @EnableScheduling                                                                    |
| @Scheduled                                                                           |
| @Component                                                                           |
| @RestController                                                                      |
| @RequestMapping                                                                      |
| @PostMapping                                                                         |
| @GetMapping                                                                          |
| @PathVariable                                                                        |
| @RequestParam                                                                        |
| @Valid                                                                               |
| @RequestBody                                                                         |
| @PutMapping                                                                          |
| @PatchMapping                                                                        |
| @DeleteMapping                                                                       |
| @ControllerAdvice                                                                    |
| @ExceptionHandler                                                                    |
| @JsonFormat                                                                          |
| @Repository                                                                          |
| @NoRepositoryBean                                                                    |
| @Service                                                                             |
| @Transactional                                                                       |
| @Qualifier                                                                           |
| @Lock                                                                                |
| @Caching                                                                             |
| @Cacheable                                                                           |
| @SpringBootApplication                                                               |
| @PostConstruct                                                                       |
| @JsonTypeInfo                                                                        |
| @JsonSubTypes                                                                        |
| @JsonIgnore                                                                          |
| @JsonIgnoreProperties                                                                |
| @JsonManagedReference                                                                |
| @JsonBackReference                                                                   | 