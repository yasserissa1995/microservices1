package com.yasser.microservice1.repository;

import com.yasser.microservice1.domain.model.composite_primary_keys.idclass.Account;
import com.yasser.microservice1.domain.model.composite_primary_keys.idclass.AccountPrimary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, AccountPrimary> {

}
