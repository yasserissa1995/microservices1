package com.yasser.microservice1.repository;

import com.yasser.microservice1.domain.model.Auditing;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the EntityAuditEvent entity.
 */
public interface AuditRepository extends JpaRepository<Auditing, Long> {

    List<Auditing> findAllByEntityTypeAndEntityId(String entityType, Long entityId);

    @Query("SELECT max(a.commitVersion) FROM Auditing a where a.entityType = :type and a.entityId = :entityId")
    Integer findMaxCommitVersion(@Param("type") String type, @Param("entityId") Long entityId);

    @Query("SELECT DISTINCT (a.entityType) from Auditing a")
    List<String> findAllEntityTypes();

    Page<Auditing> findAllByEntityType(String entityType, Pageable pageRequest);

    @Query("SELECT ae FROM Auditing ae where ae.entityType = :type and ae.entityId = :entityId and " +
            "ae.commitVersion =(SELECT max(a.commitVersion) FROM Auditing a where a.entityType = :type and " +
            "a.entityId = :entityId and a.commitVersion < :commitVersion)")
    AuditEvent findOneByEntityTypeAndEntityIdAndCommitVersion(@Param("type") String type, @Param("entityId")
    Long entityId, @Param("commitVersion") Integer commitVersion);
}
