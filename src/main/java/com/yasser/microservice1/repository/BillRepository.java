package com.yasser.microservice1.repository;

import com.yasser.microservice1.domain.model.Bill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BillRepository extends JpaRepository<Bill, Long> {

    //
    //	@Query("SELECT b from ElectricityBill b where b.id = :id")
    //	Optional<ElectricityBill> findElectricityBillById(Long id);
    //
    //    @Query("SELECT b from WaterBill b where b.id = :id")
    //    Optional<WaterBill> findWaterBillById(Long id);
    //
    //    @Query("SELECT b from FuelBill b where b.id = :id")
    //    Optional<FuelBill> findFuelBillById(Long id);
    //
    //    @Query("SELECT b from InternetBill b where b.id = :id")
    //    Optional<InternetBill> findInternetBillById(Long id);
    //
    //    @Query("SELECT b from PhoneBill b where b.id = :id")
    //    Optional<PhoneBill> findPhoneBillById(Long id);
    //
    //
    //}

}