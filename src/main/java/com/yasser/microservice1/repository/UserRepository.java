package com.yasser.microservice1.repository;

import com.yasser.microservice1.domain.model.User;
import com.yasser.microservice1.domain.model.projections.UserBasicInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Repository
public interface UserRepository
        extends RevisionRepository<User, String, Integer>, JpaSpecificationExecutor<User>, JpaRepository<User, String>,
        QueryByExampleExecutor<User> {

    String USERS_BY_LOGIN_CACHE = "usersByLogin";
    String USERS_BY_EMAIL_CACHE = "usersByEmail";
    String USERS = "USERS";

    //	Limiting query results
    List<User> queryFirst10ByLastName(String lastname);

    //	Streaming query results
    Stream<User> readAllByFirstNameNotNull();

    // custom Query
    @Query("select u from User u where u.email = ?1")
    Optional<User> findByEmailAddress(String email);

    @Override
//	@EntityGraph(attributePaths = { "authorityGroups", "authorityGroups.authorities" })
    Optional<User> findById(String id);

    // projections
    Optional<UserBasicInfo> findByEmail(String email);
}
