package com.yasser.microservice1.repository;

import com.yasser.microservice1.domain.model.composite_primary_keys.embeddedid.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {

}
