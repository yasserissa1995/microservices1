package com.yasser.microservice1.repository;

import com.yasser.microservice1.domain.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, String> {

}
