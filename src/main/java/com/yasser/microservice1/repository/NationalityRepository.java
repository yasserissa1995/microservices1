package com.yasser.microservice1.repository;

import com.yasser.microservice1.domain.model.Nationality;
import com.yasser.microservice1.security.api.nationality.Nationalities;
import com.yasser.microservice1.security.api.nationality.NationalityCreateOrEdit;
import com.yasser.microservice1.security.api.nationality.NationalityDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "nationalities", path = "nationalities")
public interface NationalityRepository extends JpaRepository<Nationality, Long> {

    @NationalityDetails
    @Override
    Optional<Nationality> findById(Long aLong);

    @Nationalities
    @Override
    List<Nationality> findAll();

    @NationalityCreateOrEdit
    @Override
    <S extends Nationality> S save(S entity);
}
