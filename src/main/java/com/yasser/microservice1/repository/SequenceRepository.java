package com.yasser.microservice1.repository;

public interface SequenceRepository {

    // Example for DDL Language in SQL
    Long getNextSequence(String sequenceName);

}
