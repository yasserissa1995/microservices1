package com.yasser.microservice1.repository.imp;

import com.yasser.microservice1.domain.model.User;
import com.yasser.microservice1.repository.AuditingRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

// for hibernate-envers
@Repository
public class UserAuditingRepositoryImp implements AuditingRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional(readOnly = true)
    public synchronized List<Object[]> findAll() {

        AuditReader auditReader = AuditReaderFactory.get(em);

        return (List<Object[]>) auditReader.createQuery().forRevisionsOfEntity(User.class, true, true)
                .addProjection(AuditEntity.property("id"))
                .addProjection(AuditEntity.property("login"))
                .addProjection(AuditEntity.property("firstName"))
                .addProjection(AuditEntity.property("lastName"))
                .addProjection(AuditEntity.property("email"))
                .addProjection(AuditEntity.property("createdBy"))
                .addProjection(AuditEntity.property("createdDate"))
                .addProjection(AuditEntity.revisionNumber())
                .addProjection(AuditEntity.revisionType())
                //			 .addProjection(AuditEntity.revisionNumber().max()) // get last revision
                .getResultList();

    }

    @Override
    @Transactional(readOnly = true)
    public List<Object[]> findById(String userId) {

        AuditReader auditReader = AuditReaderFactory.get(em);

        return ((List<Object[]>) auditReader.createQuery().forRevisionsOfEntity(User.class, true, true)
                .addProjection(AuditEntity.property("id"))
                .addProjection(AuditEntity.property("login"))
                .addProjection(AuditEntity.property("firstName"))
                .addProjection(AuditEntity.property("lastName"))
                .addProjection(AuditEntity.property("email"))
                .addProjection(AuditEntity.property("createdBy"))
                .addProjection(AuditEntity.property("createdDate"))
                .addProjection(AuditEntity.revisionNumber())
                .addProjection(AuditEntity.revisionType()).add(AuditEntity.id().eq(userId))
                .getResultList());

    }
}
