package com.yasser.microservice1.repository.imp;

import com.yasser.microservice1.domain.model.Bill;
import com.yasser.microservice1.repository.AuditingRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

// for hibernate-envers
@Repository
public class BillAuditingRepositoryImp implements AuditingRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional(readOnly = true)
    public synchronized List findAll() {

        AuditReader auditReader = AuditReaderFactory.get(em);

        return auditReader.createQuery().forRevisionsOfEntity(Bill.class, true, true)
                .addProjection(AuditEntity.property("billStatus"))
                .addProjection(AuditEntity.property("receiptId"))
                .addProjection(AuditEntity.property("amount"))
                .addProjection(AuditEntity.revisionNumber().max())
                .getResultList();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Object> findById(String id) {

        return null;
    }

}
