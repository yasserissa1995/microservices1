package com.yasser.microservice1.repository.imp;

import com.yasser.microservice1.exceptions.type.BadRequestException;
import com.yasser.microservice1.repository.SequenceRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.PersistenceException;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

// for DDL language in sql
@Repository
public class SequenceRepositoryImp implements SequenceRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public synchronized Long getNextSequence(String sequenceName) {

        try {
            return ((BigDecimal) em.createNativeQuery("select " + sequenceName + ".nextval from dual").getSingleResult()).longValueExact();

        } catch (PersistenceException ex) {

            createSequence(sequenceName);
            getNextSequence(sequenceName);
        }

        return null;
    }

    @Transactional
    public synchronized void createSequence(@NotNull String sequenceName) {

        String query = "CREATE SEQUENCE ".concat(sequenceName.trim())
                .concat(" MINVALUE 1 MAXVALUE 9999999 START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE");

        try {

            em.createNativeQuery(query).executeUpdate();

        } catch (Exception ex) {

            throw new BadRequestException("A sequence cannot be created");
        }
    }
}
