package com.yasser.microservice1.repository;

import java.util.List;

public interface AuditingRepository<T> {

    //	Object[] getAuditing();

    List<Object[]> findAll();

    List<Object[]> findById(String id);

}
