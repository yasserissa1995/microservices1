package com.yasser.microservice1.exceptions.type;

public class FormMetaStructureException extends RuntimeException {

    public FormMetaStructureException(String message) {
        super(message);
    }

}
