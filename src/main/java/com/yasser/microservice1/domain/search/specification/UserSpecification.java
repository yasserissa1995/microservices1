package com.yasser.microservice1.domain.search.specification;

import com.yasser.microservice1.domain.model.User;
import com.yasser.microservice1.domain.model.dto.UserSearchDTO;

import java.util.ArrayList;
import java.util.List;

public class UserSpecification extends TsSpecification<User> {

    public UserSpecification(SearchCriteria criteria) {
        super(criteria);
    }

    public static List<TsSpecification> specificationBuilder(UserSearchDTO userSearchDTO) {

        if (userSearchDTO == null) {
            return null;
        }

        List<TsSpecification> specifications = new ArrayList<>();

        if (userSearchDTO.getUsername() != null && !userSearchDTO.getUsername().trim().isEmpty()) {
            specifications.add(
                    new UserSpecification(
                            new SearchCriteria("login", SearchCriteria.Operation.LIKE.toString(), userSearchDTO.getUsername().trim())));
        }

        if (userSearchDTO.getEmail() != null && !userSearchDTO.getEmail().trim().isEmpty()) {
            specifications.add(
                    new UserSpecification(
                            new SearchCriteria("email", SearchCriteria.Operation.LIKE.toString(), userSearchDTO.getEmail().trim())));
        }

        if (userSearchDTO.getFirstName() != null && !userSearchDTO.getFirstName().trim().isEmpty()) {
            specifications.add(
                    new UserSpecification(new SearchCriteria("firstName", SearchCriteria.Operation.LIKE.toString(),
                            userSearchDTO.getFirstName().trim())));
        }

        if (userSearchDTO.getLastName() != null && !userSearchDTO.getLastName().trim().isEmpty()) {
            specifications.add(
                    new UserSpecification(
                            new SearchCriteria("lastName", SearchCriteria.Operation.LIKE.toString(), userSearchDTO.getLastName().trim())));
        }

        return specifications;
    }

}