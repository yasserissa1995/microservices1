package com.yasser.microservice1.domain.search.matcher;

import com.yasser.microservice1.domain.model.User;
import com.yasser.microservice1.domain.model.dto.UserSearchDTO;
import com.yasser.microservice1.util.mapper.UserMapper;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;

public class UserExampleMatcher {

    private UserExampleMatcher() {
    }

    public static Example<User> matchingAll(UserSearchDTO userSearchDTO) {

        User user = UserMapper.userSearchDTOToUser(userSearchDTO);

        return Example.of(user, ExampleMatcher.matchingAll().withIgnoreNullValues().withIgnoreCase());
    }
}