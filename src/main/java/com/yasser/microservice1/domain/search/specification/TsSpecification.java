package com.yasser.microservice1.domain.search.specification;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public class TsSpecification<T> implements Specification<T> {

    private final SearchCriteria criteria;

    public TsSpecification(SearchCriteria criteria) {
        this.criteria = criteria;
    }

    public static Specification build(List<TsSpecification> specifications) {
        Specification specification = null;

        if (!specifications.isEmpty()) {
            for (Specification s : specifications) {
                specification = Specification.where(specification)
                        .and(s);

            }

        }
        return specification;
    }

    @Override
    public Predicate toPredicate
            (Root<T> root, CriteriaQuery<?> q, CriteriaBuilder builder) {

        if (criteria.getOperation().equalsIgnoreCase(">")) {
            return builder.greaterThanOrEqualTo(
                    root.get(criteria.getKey()), criteria.getValue().toString());
        } else if (criteria.getOperation().equalsIgnoreCase("<")) {
            return builder.lessThanOrEqualTo(
                    root.get(criteria.getKey()), criteria.getValue().toString());
        } else if (criteria.getOperation().equalsIgnoreCase("=")) {
            return builder.equal(root.get(criteria.getKey()), criteria.getValue());

        } else if (criteria.getOperation().equalsIgnoreCase(":")) {
            if (root.get(criteria.getKey()).getJavaType() == String.class) {
                return builder.like(
                        root.get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else {
                return builder.equal(root.get(criteria.getKey()), criteria.getValue());
            }
        }
        return null;
    }
}
