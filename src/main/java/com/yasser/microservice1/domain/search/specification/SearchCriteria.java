package com.yasser.microservice1.domain.search.specification;


import com.yasser.microservice1.util.DateUtil;
import com.yasser.microservice1.util.ReflectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.time.temporal.TemporalAccessor;
import java.util.*;
import java.util.stream.Collectors;

//@DTO
public class SearchCriteria implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(SearchCriteria.class);
    private final List<String> rawConds = new ArrayList<>();
    private final Map<String, String> stackoverflowStopper = new HashMap<>();
    private String dateFormat;
    private String entity;
    private Operator operator = Operator.AND;
    private Map<String, Param> params = new HashMap<>();
    private Map<String, Range> ranges = new HashMap<>();
    private String operation;
    private String key;
    private Object value;

    public SearchCriteria(String entity, String dateFormat) {
        this.entity = entity;
        this.dateFormat = dateFormat;
        generateBody();

    }

    public SearchCriteria(String entity) {
        this.entity = entity;
        this.dateFormat = DateUtil.DEFAULT_DATE_TIME_FORMAT;
        generateBody();

    }

    public SearchCriteria(String key, String operation, Object value) {
        this.operation = operation;
        this.key = key;
        this.value = value;
    }

    private void generateBody() {

        Map<String, Field> fileds = ReflectionUtil.getClassFields(entity, 6);

        fileds.forEach((p, f) -> {
            boolean isDate = TemporalAccessor.class.isAssignableFrom(f.getType()) || Date.class.isAssignableFrom(f.getType());
            this.params.put(p, new Param(isDate, p, new ArrayList<>()));
            this.ranges.put(p, new Range(isDate, p, null, null));
        });

    }

    public void addRawCondition(String cond) {

        this.rawConds.add(cond);

    }

    @Override
    public String toString() {

        String query = "";
        String params = this.params.values().stream().filter(param -> !"".equals(param.toString())).map(param -> param.toString())
                .collect(Collectors.joining(" " + this.operator.name() + " "));
        String ranges = this.ranges.values().stream().filter(range -> !"".equals(range.toString())).map(range -> range.toString())
                .collect(Collectors.joining(" " + this.operator.name() + " "));

        String rawConds = this.rawConds.stream().filter(param -> !param.isEmpty())
                .collect(Collectors.joining(" " + this.operator.name() + " "));

        query = Arrays.asList(rawConds, params, ranges).stream().filter(param -> !param.isEmpty())
                .collect(Collectors.joining(" " + this.operator.name() + " "));

        log.debug("Query : " + query);
        return query;

    }

    public List<String> getParamsForEnetity() {
        return new ArrayList(params.values());
    }

    public List<String> getRangesForEnetity() {
        return new ArrayList(ranges.values());
    }

    public Map<String, Param> getParams() {
        return params;
    }

    public void setParams(Map<String, Param> params) {
        this.params = params;
    }

    public Map<String, Range> getRanges() {
        return ranges;
    }

    public void setRanges(Map<String, Range> ranges) {
        this.ranges = ranges;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public enum Operation {
        EQUAL, LIKE, GREATER_THAN, GREATER_THAN_EQ, LESS_THAN, LESS_THAN_EQ
    }

    public enum Operator {
        AND, OR
    }

    public class Param {

        private boolean date;
        private String name;
        private String fullSearch;

        private List<Object> values;

        public Param(boolean date, String name, List<Object> values) {
            this.date = date;
            this.name = name;
            this.values = values;
        }

        public boolean isDate() {
            return date;
        }

        public void setDate(boolean date) {
            this.date = date;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Object> getValues() {
            return values;
        }

        public void setValues(List<Object> values) {
            this.values = values;
        }

        public Object getValue() {
            if (this.values == null || this.values.isEmpty()) {
                return null;
            }
            return this.values.get(0);
        }

        public void setValue(Object value) {
            if (Objects.isNull(value)) {
                this.values = new ArrayList<>();
                return;
            }

            this.values = new ArrayList<>();

            this.values.add(0, value);
        }

        public String getFullSearch() {
            return fullSearch;
        }

        public void setFullSearch(String fullSearch) {
            this.fullSearch = fullSearch;
        }

        @Override
        public String toString() {
            if (!fullSearch.isEmpty()) {
                return ("( " + name + ":" + "*" + fullSearch + "*" + ") ").replace("/", "\\/");

            }
            if (name.isEmpty() || values.isEmpty()) {
                return "";
            } else {
                if (isDate()) {
                    return "( " + name + ":" + String.join(" OR ",
                            this.values.stream().map(o -> "\"" + DateUtil.fromat((TemporalAccessor) o, dateFormat) + "\"")
                                    .collect(Collectors.toList())) + ") ";

                } else {
                    return ("( " + name + ":" + String.join(" OR ",
                            this.values.stream().map(o -> o.toString()).collect(Collectors.toList())) + ") ").replace("/", "\\/");
                }

            }

        }
    }

    public class Range {

        private boolean date;
        private String name;
        private Object from;
        private Object to;

        public Range(boolean date, String name, Object from, Object to) {
            this.date = date;
            this.name = name;
            this.from = from;
            this.to = to;
        }

        public boolean isDate() {
            return date;
        }

        public void setDate(boolean date) {
            this.date = date;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Object getFrom() {
            return from;
        }

        public void setFrom(Object from) {
            this.from = from;
        }

        public Object getTo() {
            return to;
        }

        public void setTo(Object to) {
            this.to = to;
        }

        @Override
        public String toString() {
            if (name.isEmpty() || from == null || from.toString().isEmpty() || to == null || to.toString().isEmpty(
            )) {
                return "";
            } else {
                if (isDate()) {
                    //TODO   return "(" + this.name + ":[" + DateUtil.fromat((Date) this.from,"yyyy-MM-dd'T'hh:mm:ssZ") + " TO " + DateUtil.fromat((Date)this.to,"yyyy-MM-dd'T'hh:mm:ssZ") + "] )";
                    return "(" + this.name + ":[\"" + DateUtil.fromat((TemporalAccessor) this.from, dateFormat) + "\" TO \""
                            + DateUtil.fromat((TemporalAccessor) this.to, dateFormat) + "\"] )";

                } else {
                    return "(" + this.name + ":[" + this.from + " TO " + this.to + "] )";
                }
            }
        }
    }

    // public static void main(String [] args){
    //     SearchCriteria sc= new SearchCriteria(Ticket.class.getName());

    //        sc.params.get("owner.email").vlaues.add("alomoush@gmail.com");
    //        sc.params.get("type.mainType.deleted").vlaues.add("true");
    //        sc.params.get("type.specialists").vlaues.add("sp1");
    //        sc.params.get("type.specialists").vlaues.add("sp222");
    //
    //        sc.ranges.get("creatationDate").setFrom(new Date().toString());
    //        sc.ranges.get("creatationDate").setTo(new Date().toString());
    //
    //        sc.ranges.get("estimateInHours").setFrom("1");
    //        sc.ranges.get("estimateInHours").setTo("2");

    // System.out.println(sc);

}

//}
/*
  class Car {
    private Person driver;
    private String color;
    private String model;


  }


  class Person extends BaseEntity{
    private License licnse;
    private String name;
      private String age;
      @Override
      public Object getId() {
          return null;
      }
}


class License extends BaseEntity{
    private Country country;
    private Person secondDriver;

    private String year;
    private String level;
    @Override
    public Object getId() {
        return null;
    }
}

class Country extends BaseEntity {

    private String name;
    private String container;

    @Override
    public Object getId() {
        return null;
    }
}*/
