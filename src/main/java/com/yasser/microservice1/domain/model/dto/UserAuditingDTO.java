package com.yasser.microservice1.domain.model.dto;

import com.yasser.microservice1.domain.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public class UserAuditingDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String login;
	private String firstName;
	private String lastName;
	private String email;
	private String reverbType;
	private Integer revisionNumber;
	private String createdBy;
	private LocalDateTime createdDate;
	private Map<String, Map<String, String>> updatedFields = new HashMap<>();

	public UserAuditingDTO(User user) {
		this.id = user.getId();
		this.login = user.getLogin();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.email = user.getEmail();
		this.createdBy = user.getCreatedBy();
		this.createdDate = user.getCreatedDate();
	}
}
