package com.yasser.microservice1.domain.model.enums;

public enum DocumentType {

	PDF("Pdf"),
	DOC("Document"),
	HTML("Hmtl"),
	TXT("Text");

	private final String value;

	DocumentType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}