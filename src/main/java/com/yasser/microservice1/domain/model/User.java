package com.yasser.microservice1.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Table;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.springframework.data.domain.Persistable;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "USER")
@Setter
@Getter
//@ToString
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Where(clause = "active = true")
public class User extends AbstractAuditingEntity implements Serializable, Persistable<String> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    @Column(name = "VERSION_ID")
    @Version
    private Long versionId;

    @Audited
    @NotNull
    @Size(min = 1, max = 50)
    @Column(length = 50, unique = true, nullable = false)
    private String login;

    @Audited
    @NotNull
    @Size(min = 8, max = 100, message = "at least 8 characters")
    @Column(length = 100, unique = false, nullable = false)
    private String password;

    @Audited
    @Size(max = 50)
    @Column(name = "FIRST_NAME", length = 50)
    private String firstName;

    @Audited
    @Size(max = 50)
    @Column(name = "LAST_NAME", length = 50)
    private String lastName;

    @Audited
    @Email
    @Size(min = 8, max = 254)
    @Column(length = 254, unique = true)
    private String email;

    @Audited
    @Column(name = "birth_date")
    private LocalDate birthDate;

    @NotAudited
    @Formula("(TIMESTAMPDIFF(YEAR,birth_date,CURDATE()))")
    private Integer age;

    @Audited
    @Column(name = "active")
    private Boolean active;

    @NotAudited
    @NotFound(action = NotFoundAction.IGNORE) // IGNORE lazy initialize exceptions
    @ManyToMany(mappedBy = "users", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<AuthorityGroup> authorityGroups = new HashSet<>();

    @Override
    //	@ApiModelProperty(hidden = true)
    @JsonIgnore
    public boolean isNew() {
        return this.id == null || this.id.trim().isEmpty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof User)) {
            return false;
        }
        return id != null && id.equals(((User) o).id);
    }

    @Override
    public int hashCode() {

        return 31;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", login='" + login + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", birthDate=" + birthDate +
                ", age=" + age +
                ", active=" + active +
                '}';
    }
}