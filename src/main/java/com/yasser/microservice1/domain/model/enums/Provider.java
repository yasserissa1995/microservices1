package com.yasser.microservice1.domain.model.enums;

public enum Provider {

	ZAIN("Zain"),
	ORANGE("Orange"),
	UMNIAH("Umniah");

	private final String value;

	Provider(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
