package com.yasser.microservice1.domain.model.enums;

public enum AuditAction {
	CREATE("Create"),
	UPDATE("Update"),
	READ("Read"),
	DELETE("Delete"),
	UNKNOWN("Unknown");

	private final String value;

	AuditAction(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
