package com.yasser.microservice1.domain.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.yasser.microservice1.domain.model.enums.BillStatus;
import com.yasser.microservice1.domain.model.enums.BillType;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.io.Serializable;

@Setter
@Getter
@ToString
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
@Table(name = "BILL")
@Inheritance(strategy = InheritanceType.JOINED)

//@DiscriminatorColumn(name = "BILL_TYPE", discriminatorType = DiscriminatorType.STRING)
// used on the root entity class to specify the discriminator column attributes.

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ElectricityBill.class, name = "ElectricityBill"),
        @JsonSubTypes.Type(value = WaterBill.class, name = "WaterBill"),
        @JsonSubTypes.Type(value = FuelBill.class, name = "FuelBill"),
        @JsonSubTypes.Type(value = InternetBill.class, name = "InternetBill"),
        @JsonSubTypes.Type(value = PhoneBill.class, name = "PhoneBill")
})
public abstract class Bill implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "ID")
    private Long id;

    @Column(name = "VERSION_ID")
    @Version
    private Long versionId;

    //	@Column(name = "sequence", nullable = false)
    private Long sequence;

    @NotNull
    @Column(name = "NUMBER", nullable = false)
    private String number;

    @NotNull
    @Column(name = "BILL_TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private BillType billType;

    @NotNull
    @Column(name = "BILL_AMOUNT", nullable = false)
    private Double amount;

    @Basic(optional = true)
    @Column(name = "RECEIPT_ID", length = 30)
    private String receiptId;

    @Basic(optional = false)
    @Column(name = "STATUS", length = 50)
    @Enumerated(EnumType.STRING)
    private BillStatus billStatus;


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Bill)) {
            return false;
        }
        return id != null && id.equals(((Bill) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return 31;
    }
}