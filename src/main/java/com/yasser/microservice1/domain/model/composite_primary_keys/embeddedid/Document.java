package com.yasser.microservice1.domain.model.composite_primary_keys.embeddedid;

import com.yasser.microservice1.domain.model.TransField;
import com.yasser.microservice1.domain.model.converter.TransFieldAttConverter;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.io.Serializable;

@Entity
@Table(name = "doc")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Setter
@Getter
@ToString
public class Document implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private DocumentId documentId;

    @Column(name = "DESCRIPTION", length = 500)
    @Convert(converter = TransFieldAttConverter.class)
    @Size(max = 500)
    private TransField desc = new TransField();

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }

        if (!(o instanceof Document)) {
            return false;
        }

        return documentId != null &&
                documentId.getId().equals(((Document) o).getDocumentId().getId()) &&
                documentId.getDocumentType().equals(((Document) o).getDocumentId().getDocumentType());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return 31;
    }
}