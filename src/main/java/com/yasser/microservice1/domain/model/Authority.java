package com.yasser.microservice1.domain.model;

import com.yasser.microservice1.domain.model.converter.TransFieldAttConverter;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "AUTHORITY")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Authority implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "name", length = 50, unique = true)
    private String name;

    @Column(name = "DESCRIPTION", length = 500)
    @Convert(converter = TransFieldAttConverter.class)
    private @Size(max = 500) TransField desc = new TransField();


    @ManyToMany(mappedBy = "authorities")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<AuthorityGroup> authorityGroups;

    public Authority() {
    }

    public Authority(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Set<AuthorityGroup> getAuthorityGroups() {
        return authorityGroups;
    }

    public void setAuthorityGroups(Set<AuthorityGroup> authorityGroups) {
        this.authorityGroups = authorityGroups;
    }

    public TransField getDesc() {
        return desc;
    }

    public void setDesc(TransField desc) {
        this.desc = desc;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Authority)) {
            return false;
        }
        return Objects.equals(id, ((Authority) o).id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Authority{" +
                "name='" + name + '\'' +
                "description='" + desc + '\'' +
                "}";
    }
}
