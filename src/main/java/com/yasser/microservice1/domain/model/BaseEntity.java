package com.yasser.microservice1.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PostLoad;
import jakarta.persistence.PostPersist;
import jakarta.persistence.Transient;

import java.io.Serializable;
import java.util.Objects;

@MappedSuperclass
public abstract class BaseEntity extends AbstractAuditingEntity implements Serializable {

    @Transient
    protected Long index;
    @Transient
    @JsonSerialize
    private boolean persisted = false;
    @Transient
    @JsonSerialize
    private boolean markedForDeletion = false;

    public BaseEntity() {
    }

    public BaseEntity(Long index) {
        this.index = index;
    }

    @JsonIgnore
    public Object getRid() {
        return this.getId();
    }

    public abstract Object getId();

    public boolean isPersisted() {
        return persisted;
    }

    public void setPersisted(boolean persisted) {
        this.persisted = persisted;
    }

    @PostLoad
    @PostPersist
    public void setPersisted() {
        persisted = true;
    }

    @Override
    public String toString() {
        return getClass().getName() + "[ id=" + getIndex() + " ]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        BaseEntity that = (BaseEntity) o;
        return Objects.equals(getIndex(), that.getIndex());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIndex());
    }

    public boolean isMarkedForDeletion() {
        return markedForDeletion;
    }

    public void setMarkedForDeletion(boolean markedForDeletion) {
        this.markedForDeletion = markedForDeletion;
    }

    public Long getIndex() {
        if (Objects.isNull(index)) {
            if (getId() instanceof Long) {
                index = (Long) getRid();
            } else if (getId() instanceof String) {
                index = Long.getLong(String.valueOf(getRid()));
            }
        }
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }
}
