package com.yasser.microservice1.domain.model;

import com.yasser.microservice1.domain.model.converter.TransFieldAttConverter;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.io.Serializable;

@Entity
@Table(name = "NATIONALITY")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Setter
@Getter
@ToString
public class Nationality extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "VERSION_ID")
    @Version
    private Long versionId;

    @NotNull
    @Column(name = "name", length = 255)
    @Convert(converter = TransFieldAttConverter.class)
    @Size(min = 1, max = 255)
    private TransField name = new TransField();

    @Column(name = "nation_code")
    private Integer code;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Nationality)) {
            return false;
        }
        return id != null && id.equals(((Nationality) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return 31;
    }
}