package com.yasser.microservice1.domain.model.composite_primary_keys.embeddedid;

import com.yasser.microservice1.domain.model.enums.DocumentType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class DocumentId implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator", sequenceName = "YOUR_DB_SEQ", allocationSize = 1)
    private Long id;

    @Column(name = "DOCUMENT_TYPE", nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private DocumentType documentType;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        DocumentId documentId = (DocumentId) o;
        return id.equals(documentId.id) && documentType.equals(documentId.documentType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, documentType);
    }

}