package com.yasser.microservice1.domain.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "WATER_BILL")
@DiscriminatorValue("WaterBill")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Setter
@Getter
@ToString
@PrimaryKeyJoinColumn(name = "ID", referencedColumnName = "ID")
public class WaterBill extends Bill implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "FROM_DATE", nullable = false)
    private LocalDate from;

    @NotNull
    @Column(name = "TO_DATE", nullable = false)
    private LocalDate to;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof WaterBill)) {
            return false;
        }
        return getId() != null && getId().equals(((WaterBill) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return 31;
    }
}