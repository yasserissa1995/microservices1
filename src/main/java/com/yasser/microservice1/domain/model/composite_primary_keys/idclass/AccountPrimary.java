package com.yasser.microservice1.domain.model.composite_primary_keys.idclass;

import com.yasser.microservice1.domain.model.enums.AccountType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountPrimary implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private AccountType accountType;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        AccountPrimary accountPrimary = (AccountPrimary) o;
        return id.equals(accountPrimary.id) && accountType.equals(accountPrimary.accountType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountType);
    }

}