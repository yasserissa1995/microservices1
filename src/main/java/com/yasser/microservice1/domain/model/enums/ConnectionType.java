package com.yasser.microservice1.domain.model.enums;

public enum ConnectionType {

	MOBILE("Mobile"),
	DSL("Dsl"),
	CABLE("Cable"),
	WIFI("Wifi");

	private final String value;

	ConnectionType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
