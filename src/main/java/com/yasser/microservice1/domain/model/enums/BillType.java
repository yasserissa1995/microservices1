package com.yasser.microservice1.domain.model.enums;

public enum BillType {

	ELECTRICITY_BILL("Electricity Bill"),
	WATER_BILL("Water Bill"),
	INTERNET_BILL("Internet Bill"),
	PHONE_BILL("Phone Bill"),
	FUEL_BILL("Fuel Bill");

	private final String value;

	BillType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
