package com.yasser.microservice1.domain.model.enums;

public enum BillStatus {

	NEW("New"),
	PAID("Paid");

	private final String value;

	BillStatus(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
