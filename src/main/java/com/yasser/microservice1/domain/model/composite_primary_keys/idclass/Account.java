package com.yasser.microservice1.domain.model.composite_primary_keys.idclass;

import com.yasser.microservice1.domain.model.TransField;
import com.yasser.microservice1.domain.model.converter.TransFieldAttConverter;
import com.yasser.microservice1.domain.model.enums.AccountType;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.io.Serializable;

@Entity
@Table(name = "ACCOUNT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Setter
@Getter
@ToString
@IdClass(AccountPrimary.class) // Composite Primary Keys
public class Account implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Id
    @Column(name = "ACCOUNT_TYPE")
    @Enumerated(EnumType.STRING)
    private AccountType accountType;

    @Column(name = "DESCRIPTION", length = 500)
    @Convert(converter = TransFieldAttConverter.class)
    @Size(max = 500)
    private TransField desc = new TransField();

    @NotNull
    @Column(name = "BALANCE", nullable = false)
    private Double balance;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Account)) {
            return false;
        }
        return id != null && id.equals(((Account) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return 31;
    }
}