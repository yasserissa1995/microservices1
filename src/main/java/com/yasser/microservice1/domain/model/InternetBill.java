package com.yasser.microservice1.domain.model;

import com.yasser.microservice1.domain.model.enums.ConnectionType;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "INTERNET_BILL")
@DiscriminatorValue("InternetBill")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Setter
@Getter
@ToString
@PrimaryKeyJoinColumn(name = "ID", referencedColumnName = "ID")
public class InternetBill extends Bill implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "BILL_DATE", nullable = false)
    private LocalDate date;

    @Basic(optional = false)
    @Column(name = "CONNECTION_TYPE", length = 50)
    @Enumerated(EnumType.STRING)
    private ConnectionType connectionType;

    @NotNull
    @Column(name = "SPEED", nullable = false)
    private Double speed;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof InternetBill)) {
            return false;
        }
        return getId() != null && getId().equals(((InternetBill) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return 31;
    }
}