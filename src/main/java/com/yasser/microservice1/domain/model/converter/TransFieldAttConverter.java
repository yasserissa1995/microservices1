package com.yasser.microservice1.domain.model.converter;

import com.yasser.microservice1.domain.model.TransField;
import com.yasser.microservice1.util.JSONUtil;
import jakarta.persistence.AttributeConverter;

public class TransFieldAttConverter implements AttributeConverter<TransField, String> {

	@Override
	public String convertToDatabaseColumn(TransField attribute) {
		if (attribute == null) {
			return null;
		}
		return JSONUtil.convertTransFieldToJson(attribute);
	}

	@Override
	public TransField convertToEntityAttribute(String dbData) {
		if (dbData.isEmpty()) {
			return new TransField();
		}
		return JSONUtil.convertJsonToTransField(dbData);
	}
}
