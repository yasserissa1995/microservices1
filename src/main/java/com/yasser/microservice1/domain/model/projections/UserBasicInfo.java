package com.yasser.microservice1.domain.model.projections;

public interface UserBasicInfo {

    String getFirstName();

    String getLastName();

}
