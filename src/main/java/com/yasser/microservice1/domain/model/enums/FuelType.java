package com.yasser.microservice1.domain.model.enums;

public enum FuelType {

	KAZ("Kaz"),
	GAS("Gas"),
	PETROL("Petrol"),
	DIESEL("Diesel");

	private final String value;

	FuelType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}