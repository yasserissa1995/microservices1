package com.yasser.microservice1.domain.model.enums;

public enum AccountType {

	COM("Com"), NON_COM("Non Com");

	private final String value;

	AccountType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}