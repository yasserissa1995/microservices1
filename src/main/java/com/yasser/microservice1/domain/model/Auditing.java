package com.yasser.microservice1.domain.model;

import com.yasser.microservice1.domain.model.enums.AuditAction;
import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "AUDITING")
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Auditing implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "audit_seq")
    @SequenceGenerator(name = "audit_seq", allocationSize = 1, sequenceName = "audit_seq")
    private Long id;

    @Column(name = "entity_id")
    private Long entityId;

    @Column(name = "entity_type")
    private String entityType;

    //	@Size(max = 20)
    @Column(name = "action")
    @Enumerated(EnumType.STRING)
    private AuditAction action;

    //	@Nationalized
    //@Lob // Blob for array of byte and Clob for array of char (String also set of chars) : detected automatically
    @Column(name = "entity_value", columnDefinition = "TEXT") // columnDefinition -.rather than LOB
//	@Type(type="text")
    private String entityValue;

    @Column(name = "resource_uri")
    private String resourceUri;

    //	@Nationalized
    //@Lob // Blob for array of byte and Clob for array of char (String also set of chars) : detected automatically
    @Column(name = "request_value", columnDefinition = "TEXT")
    private String requestValue;

    @Column(name = "commit_version")
    private Integer commitVersion;

    @Column(name = "action_by_username")
    private String actionByUsename;

    @Column(name = "action_by_UserId")
    private String actionByUserId;

    @Column(name = "action_date")
    private LocalDateTime actionDate;

    @Column(name = "module")
    private String module;

}
