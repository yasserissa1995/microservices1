package com.yasser.microservice1.controller;

import com.yasser.microservice1.domain.model.User;
import com.yasser.microservice1.domain.model.dto.UserSearchDTO;
import com.yasser.microservice1.domain.model.projections.UserBasicInfo;
import com.yasser.microservice1.exceptions.type.BadRequestException;
import com.yasser.microservice1.security.api.user.UserCreate;
import com.yasser.microservice1.security.api.user.UserDetails;
import com.yasser.microservice1.security.api.user.UserEdit;
import com.yasser.microservice1.security.api.user.Users;
import com.yasser.microservice1.service.UserService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class UserResource {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    private final UserService userService;

    public UserResource(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/users")
    @UserCreate
    public ResponseEntity<User> createUser(@Valid @RequestBody User user) {
        log.info("REST Request to save user", user);

        return ResponseEntity.of(Optional.ofNullable(userService.create(user)));
    }

    @PutMapping("/users")
    @UserEdit
    public ResponseEntity<User> editUser(@Valid @RequestBody User user) {
        log.info("REST Request to save user", user);

        if (user.getId() == null || user.getId().isEmpty()) {

            throw new BadRequestException("id connot be empty");
        }

        return ResponseEntity.of(Optional.ofNullable(userService.save(user)));
    }

    @GetMapping("/users/authorities")
    public List<String> getAuthorities() {
        log.info("REST Request to get authorities");

        return userService.getAuthorities();
    }

    @GetMapping("/users")
    @UserDetails
    public ResponseEntity<User> findById(@RequestParam(required = true, name = "id") String id) {
        log.info("REST Request to get authorities");

        return ResponseEntity.of(userService.findById(id));
    }

    @GetMapping("/users/email/custom-query")
    @UserDetails
    public ResponseEntity<User> findByEmailAddress(@RequestParam(required = true) String email) {
        log.info("REST Request to get User by email using custom query");

        return ResponseEntity.of(userService.findByEmailAddress(email));
    }

    @GetMapping("/users/email/custom-attribute")
    @UserDetails
    public ResponseEntity<UserBasicInfo> findByEmail(@RequestParam(required = true) String email) {
        log.info("REST Request to get User by email using custom attribute projection");

        return ResponseEntity.of(userService.findByEmail(email));
    }

    @GetMapping("/users/specifications/findAll")
    @Users
    public ResponseEntity<List<User>> searchUsers(@RequestBody UserSearchDTO userSearchDTO) {

        log.debug("REST request to get OrganizationUnit : {} ", "all departments");

        return ResponseEntity.ok(userService.findAll(userSearchDTO));
    }

    @GetMapping("/users/example-matcher/findAll")
    @Users
    public ResponseEntity<List<User>> matchingAll(@RequestBody UserSearchDTO userSearchDTO) {

        log.debug("REST request to get OrganizationUnit : {} ", "all departments");

        return ResponseEntity.ok(userService.matchingAll(userSearchDTO));
    }
}
