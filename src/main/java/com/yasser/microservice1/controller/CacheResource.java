package com.yasser.microservice1.controller;

import com.yasser.microservice1.service.CacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class CacheResource {

    private final Logger log = LoggerFactory.getLogger(CacheResource.class);

    private final CacheService cacheService;

    public CacheResource(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @DeleteMapping("/cache/remove")
    public ResponseEntity<Void> removeCache() {
        log.debug("REST request to clear microservice1 caches");

        cacheService.removeCache();

        return ResponseEntity.noContent().build();
    }
}
