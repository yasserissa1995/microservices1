package com.yasser.microservice1.controller;

import com.yasser.microservice1.domain.model.composite_primary_keys.embeddedid.Document;
import com.yasser.microservice1.exceptions.type.BadRequestException;
import com.yasser.microservice1.service.DocumentService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class DocumentResource {

    private final Logger log = LoggerFactory.getLogger(DocumentResource.class);

    private final DocumentService documentService;

    public DocumentResource(DocumentService documentService) {
        this.documentService = documentService;
    }

    @PostMapping(value = "/documents")
    public ResponseEntity<Document> create(@Valid @RequestBody Document document) {
        log.debug("REST apply Request");

        if (document.getDocumentId() == null) {
            throw new BadRequestException("getDocumentId must be not null");
        }

        if (document.getDocumentId().getId() != null) {
            //	throw new BadRequestException("id must be empty");
        }

        return new ResponseEntity<>(documentService.save(document), HttpStatus.OK);
    }

    @GetMapping("/documents/{id}")
    public ResponseEntity<Document> findById(@PathVariable(name = "id", required = true) Long id) {

        Optional<Document> request = documentService.findById(id);

        if (request.isPresent()) {
            return new ResponseEntity<>(request.get(), HttpStatus.OK);
        } else {
            throw new BadRequestException("not found By Id " + id);
        }
    }
}
