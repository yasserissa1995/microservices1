package com.yasser.microservice1.controller;

import com.yasser.microservice1.domain.model.composite_primary_keys.idclass.Account;
import com.yasser.microservice1.domain.model.enums.AccountType;
import com.yasser.microservice1.exceptions.type.BadRequestException;
import com.yasser.microservice1.service.AccountService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class AccountResource {

	private final Logger log = LoggerFactory.getLogger(AccountResource.class);

	private final AccountService accountService;

	public AccountResource(AccountService accountService) {
		this.accountService = accountService;
	}

	@PostMapping(value = "/accounts")
	public ResponseEntity<Account> create(@Valid @RequestBody Account account) {
		log.debug("REST apply Request");

		if (account.getId() != null) {
			throw new BadRequestException("id must be empty");
		}

		return new ResponseEntity<>(accountService.save(account), HttpStatus.OK);
	}

	@GetMapping("/accounts/{id}/{type}")
	public ResponseEntity<Account> findById(@PathVariable(name = "id", required = true) Long id,
			@PathVariable(name = "type", required = true) AccountType type) {

		Optional<Account> request = accountService.findByPrimaryId(id, type);

		if (request.isPresent()) {
			return new ResponseEntity<>(request.get(), HttpStatus.OK);
		} else {
			throw new BadRequestException("not found By Id " + id);
		}
	}

	@GetMapping("/accounts")
	public List<String> findAll() {

		return accountService.findAll().stream().map(account -> account.getAccountType().toString()).collect(Collectors.toList());

	}
}
