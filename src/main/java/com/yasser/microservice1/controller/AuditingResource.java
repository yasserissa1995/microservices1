package com.yasser.microservice1.controller;

import com.yasser.microservice1.domain.model.User;
import com.yasser.microservice1.domain.model.dto.UserAuditingDTO;
import com.yasser.microservice1.repository.UserRepository;
import com.yasser.microservice1.service.AuditingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.history.Revisions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class AuditingResource {

    private final Logger log = LoggerFactory.getLogger(AuditingResource.class);

    private final AuditingService auditingService;
    private final UserRepository userRepository;

    public AuditingResource(AuditingService auditingService,
                            UserRepository userRepository) {
        this.auditingService = auditingService;
        this.userRepository = userRepository;
    }

    @GetMapping("/auditing/hibernate/users")
    public List<UserAuditingDTO> getHibernateAuditingUsers() {
        log.info("REST Request to get Auditing Users");

        return auditingService.findAll();
    }

    @GetMapping("/auditing/hibernate/users/by-user")
    public List<UserAuditingDTO> getHibernateAuditingUsers(@RequestParam(required = true, name = "userId") String userId) {
        log.info("REST Request to get Auditing Users");

        return auditingService.findByUserId(userId);
    }

//	@GetMapping("/auditing//users")
//	public List<UserAuditingDTO> getAuditingUsers() {
//		log.info("REST Request to get Auditing Users");
//
//		return userRepository.findLastChangeRevision()
//	}

    @GetMapping("/auditing/jpa/users/by-user")
    public List<UserAuditingDTO> getJPAAuditingByUser(@RequestParam(required = true, name = "userId") String userId) {
        log.info("REST Request to get Auditing Users");

        Revisions<Integer, User> result = userRepository.findRevisions(userId).reverse();
        List<UserAuditingDTO> userAuditingDTOS = new ArrayList<>();

        result.forEach(integerUserRevision -> {

            UserAuditingDTO userAuditingDTO = new UserAuditingDTO();
            userAuditingDTO.setId(integerUserRevision.getEntity().getId());
            userAuditingDTO.setLogin(integerUserRevision.getEntity().getLogin());
            userAuditingDTO.setFirstName(integerUserRevision.getEntity().getFirstName());
            userAuditingDTO.setLastName(integerUserRevision.getEntity().getLastName());
            userAuditingDTO.setEmail(integerUserRevision.getEntity().getEmail());
            userAuditingDTO.setCreatedBy(integerUserRevision.getEntity().getCreatedBy());
            userAuditingDTO.setCreatedDate(integerUserRevision.getEntity().getCreatedDate());
            userAuditingDTO.setRevisionNumber(integerUserRevision.getRevisionNumber().get());
            userAuditingDTO.setReverbType(integerUserRevision.getMetadata().getRevisionType().name());
            userAuditingDTOS.add(userAuditingDTO);
        });

        return userAuditingDTOS;

    }


    // jpa does not supported
    @GetMapping("/auditing/jpa/users")
    public List<UserAuditingDTO> getJPAAuditing() {
        log.info("REST Request to get Auditing Users");

        return null;

    }


}
