package com.yasser.microservice1.controller;

import com.yasser.microservice1.domain.model.Bill;
import com.yasser.microservice1.domain.model.ElectricityBill;
import com.yasser.microservice1.exceptions.type.BadRequestException;
import com.yasser.microservice1.service.BillService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class BillResource {

    private final Logger log = LoggerFactory.getLogger(BillResource.class);

    private final BillService billService;

    public BillResource(BillService billService) {
        this.billService = billService;
    }

    @PostMapping(value = "/bills",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<? extends Bill> create(@RequestBody Bill bill) {
        log.debug("REST apply Request");

        if (bill.getId() != null) {
            throw new BadRequestException("id must be empty");
        }

        return new ResponseEntity<>(billService.create(bill), HttpStatus.OK);
    }

    @PostMapping(value = "/bills/elect",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<? extends Bill> create(@Valid @RequestBody ElectricityBill bill) {
        log.debug("REST apply Request");

        if (bill.getId() != null) {
            throw new BadRequestException("id must be empty");
        }

        return new ResponseEntity<>(billService.create(bill), HttpStatus.OK);
    }

    @PutMapping(value = "/bills",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<? extends Bill> edit(@Valid @RequestBody Bill bill) {
        log.debug("REST apply Request");

        if (bill.getId() == null) {
            throw new BadRequestException("id is not optional");
        }

        return new ResponseEntity<>(billService.edit(bill), HttpStatus.OK);
    }

    @GetMapping("/bills/{id}")
    public ResponseEntity<Bill> findById(@PathVariable(name = "id", required = true) Long id) {

        Optional<? extends Bill> request = billService.findById(id);

        if (request.isPresent()) {
            return new ResponseEntity<>(request.get(), HttpStatus.OK);
        } else {
            throw new BadRequestException("not found By Id " + id);
        }
    }

}
