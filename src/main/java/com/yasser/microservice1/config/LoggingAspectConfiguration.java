package com.yasser.microservice1.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.yasser.microservice1.config.aop.audit.LoggingAspect;
import org.springframework.context.annotation.*;
import org.springframework.scheduling.annotation.EnableAsync;
import org.zalando.problem.jackson.ProblemModule;
import org.zalando.problem.violations.ConstraintViolationProblemModule;

@Configuration
@EnableAspectJAutoProxy
@EnableAsync
public class LoggingAspectConfiguration {

    @Bean
    @Profile("AUDIT") // run when AUDIT profile is active
    public LoggingAspect loggingAspect() {
        return new LoggingAspect();
    }

    @Bean
    @Primary // give priority high to a bean when there are multiple beans of the same type.
    public ObjectMapper objectMapper() {

        ObjectMapper objectMapper = new ObjectMapper().registerModules(
                new JavaTimeModule(),
                new ProblemModule(),
                new ConstraintViolationProblemModule()
        );

        objectMapper.configure(JsonParser.Feature.ALLOW_COMMENTS,
                true); // allow comment to be sent inside json ex { //this is a comment inside a json }

        return objectMapper;
    }
}
