package com.yasser.microservice1.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.envers.repository.support.EnversRevisionRepositoryFactoryBean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(repositoryFactoryBeanClass = EnversRevisionRepositoryFactoryBean.class, basePackages = {"com.yasser.microservice1.repository"})
@EntityScan(basePackages = "com.yasser.microservice1")
@EnableJpaAuditing(auditorAwareRef = "springSecurityAuditorAware") // for get requester name
@EnableTransactionManagement
public class DatabaseConfiguration {

}
