package com.yasser.microservice1.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final Logger log = LoggerFactory.getLogger(CacheConfiguration.class);

    private final CacheManager cacheManager;

    public CacheConfiguration(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    public synchronized void removeCaches() {
        log.debug("Request to all removeCaches");

        cacheManager.getCacheNames().forEach(cacheName -> cacheManager.getCache(cacheName).clear());
    }
}