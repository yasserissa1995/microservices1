package com.yasser.microservice1.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;

@Configuration
@EnableScheduling
@Profile("job")
public class SchedulingConfiguration {

    private final Logger log = LoggerFactory.getLogger(SchedulingConfiguration.class);

    @Scheduled(fixedDelay = 30000)
    public void scheduleFixedDelayTask() {

        System.out.println("Schedule Task - " + LocalDateTime.now());
    }
}
