package com.yasser.microservice1.security.api.nationality;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
//@PreAuthorize("hasRole('"+ AuthoritiesConstants.NATIONALITY_CREATE+"') or hasRole('"+ AuthoritiesConstants.NATIONALITY_EDIT+"'")
public @interface NationalityCreateOrEdit {

}
