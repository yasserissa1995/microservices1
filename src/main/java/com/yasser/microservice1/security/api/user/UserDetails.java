package com.yasser.microservice1.security.api.user;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
//@PreAuthorize("hasRole('"+ AuthoritiesConstants.USER_DETAILS+"')")
public @interface UserDetails {

}
