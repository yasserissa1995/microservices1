package com.yasser.microservice1.security.api;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    // admins
    public static final String ADMIN = "ROLE_ADMIN";

    // users
    public static final String USER_DETAILS = "ROLE_USER_DETAILS";
    public static final String USERS = "ROLE_USERS";
    public static final String USER_CREATE = "ROLE_USER_CREATE";
    public static final String USER_EDIT = "ROLE_USER_EDIT";

    // nationalities

    public static final String NATIONALITY_DETAILS = "ROLE_NATIONALITY_DETAILS";
    public static final String NATIONALITIES = "ROLE_NATIONALITIES";
    public static final String NATIONALITY_CREATE = "ROLE_NATIONALITY_CREATEE";
    public static final String NATIONALITY_EDIT = "ROLE_NATIONALITY_EDIT";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    // constructor
    private AuthoritiesConstants() {
    }
}
