package com.yasser.microservice1.service;

import com.yasser.microservice1.domain.model.composite_primary_keys.embeddedid.Document;
import com.yasser.microservice1.repository.DocumentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class DocumentService {

    private final Logger log = LoggerFactory.getLogger(DocumentService.class);

    private final DocumentRepository documentRepository;

    public DocumentService(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }

    public Document save(Document document) {
        log.debug("save document");

        return documentRepository.save(document);
    }

    @Transactional(readOnly = true)
    public Optional<Document> findById(Long id) {
        log.debug("get account : {} ", id);

        return documentRepository.findById(id);
    }
}