package com.yasser.microservice1.service;

import com.yasser.microservice1.domain.model.dto.UserAuditingDTO;
import com.yasser.microservice1.repository.AuditingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AuditingService {

	private final Logger log = LoggerFactory.getLogger(AuditingService.class);

	@Autowired
	@Qualifier("userAuditingRepositoryImp")
	private AuditingRepository auditingRepository;

	public List<UserAuditingDTO> findAll() {
		log.debug("find all Data");

		List<Object[]> data = auditingRepository.findAll();
		List<UserAuditingDTO> userAuditingDTOS = new ArrayList<>();

		for (int i = 0; i < data.size(); i++) {

			UserAuditingDTO userAuditingDTO = new UserAuditingDTO();

			if (data.get(i) != null && data.get(i).length > 2) {

				// must be   flow this  order field  =>  first name , last name , email , username
				userAuditingDTO.setId((String) data.get(i)[0]);
				userAuditingDTO.setLogin((String) data.get(i)[1]);
				userAuditingDTO.setFirstName((String) data.get(i)[2]);
				userAuditingDTO.setLastName((String) data.get(i)[3]);
				userAuditingDTO.setEmail((String) data.get(i)[4]);
				userAuditingDTO.setCreatedBy((String) data.get(i)[5]);
				userAuditingDTO.setCreatedDate((LocalDateTime) data.get(i)[6]);
				userAuditingDTO.setRevisionNumber((Integer) data.get(i)[7]);
				if (data.get(i)[8] instanceof org.hibernate.envers.RevisionType revisionType) {

					userAuditingDTO.setReverbType(revisionType.name());
				}

			}

			if (i != 0) {

				Map<String, Map<String, String>> updatedFields = new HashMap<>();
				Object[] previous = data.get(i - 1);
				Object[] current = data.get(i);

				if (!current[1].equals(previous[1])) {
					Map<String, String> innerMap = new HashMap<>();
					innerMap.put("oldValue", previous[1] + "");
					innerMap.put("newValue", current[1] + "");
					updatedFields.put("login", innerMap);
				}

				if (!current[2].equals(previous[2])) {
					Map<String, String> innerMap = new HashMap<>();
					innerMap.put("oldValue", previous[2] + "");
					innerMap.put("newValue", current[2] + "");
					updatedFields.put("firstName", innerMap);
				}

				if (!current[3].equals(previous[3])) {
					Map<String, String> innerMap = new HashMap<>();
					innerMap.put("oldValue", previous[3] + "");
					innerMap.put("newValue", current[3] + "");
					updatedFields.put("lastName", innerMap);
				}

				if (!current[4].equals(previous[4])) {
					Map<String, String> innerMap = new HashMap<>();
					innerMap.put("oldValue", previous[4] + "");
					innerMap.put("newValue", current[4] + "");
					updatedFields.put("email", innerMap);
				}

				if (!updatedFields.isEmpty()) {
					userAuditingDTO.setUpdatedFields(updatedFields);
				}
			}

			userAuditingDTOS.add(userAuditingDTO);
		}

		return userAuditingDTOS;
	}

	public List<UserAuditingDTO> findByUserId(String userId) {

		List<Object[]> data = auditingRepository.findById(userId);

		List<UserAuditingDTO> userAuditingDTOS = new ArrayList<>();

		data.forEach(objects -> {

			UserAuditingDTO userAuditingDTO = new UserAuditingDTO();

			if (objects != null && objects.length > 2) {

				// must be   flow this  order field  =>  first name , last name , email , username
				userAuditingDTO.setId((String) objects[0]);
				userAuditingDTO.setLogin((String) objects[1]);
				userAuditingDTO.setFirstName((String) objects[2]);
				userAuditingDTO.setLastName((String) objects[3]);
				userAuditingDTO.setEmail((String) objects[4]);
				userAuditingDTO.setCreatedBy((String) objects[5]);
				userAuditingDTO.setCreatedDate((LocalDateTime) objects[6]);
				userAuditingDTO.setRevisionNumber((Integer) objects[7]);
				if (objects[8] instanceof org.hibernate.envers.RevisionType revisionType) {

					userAuditingDTO.setReverbType(revisionType.name());
				}

			}

			userAuditingDTOS.add(userAuditingDTO);
		});

		return userAuditingDTOS;
	}
}