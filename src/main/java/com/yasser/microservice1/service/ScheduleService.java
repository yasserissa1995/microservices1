package com.yasser.microservice1.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ScheduleService {

    private final Logger log = LoggerFactory.getLogger(ScheduleService.class);

    @Profile("job")
    @Scheduled(fixedDelay = 10000)
    public void scheduleFixedDelayTask() {

        log.debug("Schedule Task   : {}", LocalDateTime.now());
    }
}