package com.yasser.microservice1.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yasser.microservice1.domain.model.Auditing;
import com.yasser.microservice1.domain.model.enums.AuditAction;
import com.yasser.microservice1.repository.AuditRepository;
import com.yasser.microservice1.repository.UserRepository;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Aspect for logging execution of service and repository Spring components.
 * <p>
 * it only runs with the "AUDIT" profile.
 */
@Component
public class AuditService {

	private static final Logger log = LoggerFactory.getLogger(AuditService.class);
	private final Environment env;
	@Autowired
	private AuditRepository auditingEntityRepository;
	@Autowired
	private UserRepository userEntityRepository;
	@Autowired
	private ObjectMapper objectMapper; //Jackson object mapper

	public AuditService(Environment env) {
		this.env = env;
	}

	//	@Async
	public void audit(ProceedingJoinPoint joinPoint, Object result) {

		if (log.isDebugEnabled()) {

			log.debug("Enter: {}() with argument[s] = {}", joinPoint.getSignature().getName(), Arrays.toString(joinPoint.getArgs()));
		}

		AtomicReference<AuditAction> action = new AtomicReference<>();
		AtomicReference<String> resourceUri = new AtomicReference<>();

		try {

			Annotation[] annotations = ((MethodSignature) joinPoint.getSignature()).getMethod().getDeclaredAnnotations();

			if (annotations != null && annotations.length > 0) {

				List<GetMapping> gets = new ArrayList<>();
				List<PostMapping> creates = new ArrayList<>();
				List<PutMapping> updates = new ArrayList<>();
				List<DeleteMapping> deletes = new ArrayList<>();

				Arrays.stream(annotations).forEach(annotation -> {

					if (annotation.annotationType().equals(GetMapping.class) && gets.isEmpty()) {

						gets.add((GetMapping) annotation);
						action.set(AuditAction.READ);
						resourceUri.set(((GetMapping) annotation).value()[0]);

					} else if (annotation.annotationType().equals(PostMapping.class) && creates.isEmpty()) {

						creates.add((PostMapping) annotation);
						action.set(AuditAction.CREATE);
						resourceUri.set(((PostMapping) annotation).value()[0]);

					} else if (annotation.annotationType().equals(PutMapping.class) && gets.isEmpty()) {

						updates.add((PutMapping) annotation);
						action.set(AuditAction.READ);
						resourceUri.set(((PutMapping) annotation).value()[0]);

					} else if (annotation.annotationType().equals(DeleteMapping.class) && gets.isEmpty()) {

						deletes.add((DeleteMapping) annotation);
						action.set(AuditAction.READ);
						resourceUri.set(((DeleteMapping) annotation).value()[0]);
					}
				});

				if (gets.isEmpty() && creates.isEmpty() && updates.isEmpty() && deletes.isEmpty()) {
					action.set(AuditAction.UNKNOWN);
				}

			} else {
				action.set(AuditAction.UNKNOWN);
			}

			if (result == null) {

				// aop does not return a result
				result = new Object();
			}

			Auditing auditedEntity = new Auditing();
			auditedEntity.setResourceUri(resourceUri.get());
			auditedEntity.setAction(action.get());

			if (joinPoint.getArgs() != null && joinPoint.getArgs().length > 0) {

				StringBuilder input = new StringBuilder();

				Arrays.asList(joinPoint.getArgs()).forEach(a -> {

					try {
						input.append(objectMapper.writeValueAsString(a));

					} catch (JsonProcessingException e) {

						System.out.println("the objectMapper Cannot convert " + a + " to json");
					}
				});

				auditedEntity.setRequestValue(input.toString());
			}

			prepareAuditEntity(result, auditedEntity);

		} catch (Throwable e) {

			log.error("Illegal argument: {} in {}()", Arrays.toString(joinPoint.getArgs()), joinPoint.getSignature().getName());
		}
	}

	public Auditing prepareAuditEntity(Object entity, Auditing auditedEntity) {

		// todo when security id done
		//	Optional<User> user = SecurityUtils.getCurrentUserLogin().flatMap(userEntityRepository::findOneByLogin);

		auditedEntity.setModule(env.getProperty("spring.application.name"));
		Class<?> entityClass = entity.getClass(); // Retrieve entity class with reflection

		auditedEntity.setEntityType(entityClass.getName());
		Long entityId = 0L;
		StringBuilder entityData = new StringBuilder();

		log.trace("Getting Entity Id and Content");

		try {

			if (entity instanceof ResponseEntity) {

				if (((ResponseEntity) entity).getBody() != null) {

					entity = ((ResponseEntity) entity).getBody();
				}

				entityClass = entity.getClass();
				auditedEntity.setEntityType(entityClass.getName());

			} else if (entity instanceof List<?> l) {

				if (!l.isEmpty()) {
					auditedEntity.setEntityType(l.getClass().getName() + "<" + l.get(0).getClass().getName() + ">");
				}

			}

			try {

				Field privateLongField = entityClass.getDeclaredField("id");
				privateLongField.setAccessible(true);
				entityId = (Long) privateLongField.get(entity);
				privateLongField.setAccessible(false);

			} catch (Exception t) {
				log.error("ERROR : {}", t.getMessage());

			}

			auditedEntity.setEntityId(entityId);
			auditedEntity.setActionDate(LocalDateTime.now());

			auditedEntity.setActionByUsename("System Username");
			auditedEntity.setActionByUserId("System Id");

			if (AuditAction.CREATE.equals(auditedEntity.getAction())) {

				auditedEntity.setCommitVersion(1);

			} else if (AuditAction.READ.equals(auditedEntity.getAction())) {

				auditedEntity.setCommitVersion(0);

			} else {

				calculateVersion(auditedEntity);
			}

			entityData.append(objectMapper.writeValueAsString(entity));

		} catch (IllegalArgumentException | SecurityException |
				 IOException e) {
			log.error("Exception while getting entity ID and content  : {}", e);
			// returning null as we don't want to raise an application exception here
			return null;
		}

		auditedEntity.setEntityValue(entityData.toString());

		try {
			auditedEntity = this.auditingEntityRepository.save(auditedEntity);
		} catch (Exception t) {
			log.error(" Error ", t.getMessage());

		}

		return auditedEntity;
	}

	private void calculateVersion(Auditing auditedEntity) {
		log.trace("Version calculation. for update/remove");

		Integer lastCommitVersion = auditingEntityRepository.findMaxCommitVersion(auditedEntity
				.getEntityType(), auditedEntity.getEntityId());

		log.trace("Last commit version of entity => {}", lastCommitVersion);

		if (lastCommitVersion != null && lastCommitVersion != 0) {
			log.trace("Present. Adding version..");

			auditedEntity.setCommitVersion(lastCommitVersion + 1);

		} else {

			log.trace("No entities.. Adding new version 1");

			auditedEntity.setCommitVersion(1);
		}
	}
}