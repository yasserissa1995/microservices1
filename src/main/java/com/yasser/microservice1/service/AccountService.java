package com.yasser.microservice1.service;

import com.yasser.microservice1.domain.model.composite_primary_keys.idclass.Account;
import com.yasser.microservice1.domain.model.composite_primary_keys.idclass.AccountPrimary;
import com.yasser.microservice1.domain.model.enums.AccountType;
import com.yasser.microservice1.repository.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AccountService {

	private final Logger log = LoggerFactory.getLogger(AccountService.class);

	private final AccountRepository accountRepository;

	public AccountService(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}

	public Account save(Account account) {
		log.debug("save account");

		return accountRepository.save(account);
	}

	@Transactional(readOnly = true)
	public Optional<Account> findByPrimaryId(Long id, AccountType accountType) {
		log.debug("get account ", id, accountType);

		return accountRepository.findById(new AccountPrimary(id, accountType));
	}

	@Transactional(readOnly = true)
	public List<Account> findAll() {
		log.debug("get all accounts ");

		return accountRepository.findAll();
	}

}