package com.yasser.microservice1.service;

import com.yasser.microservice1.domain.model.Authority;
import com.yasser.microservice1.domain.model.User;
import com.yasser.microservice1.domain.model.dto.UserSearchDTO;
import com.yasser.microservice1.domain.model.projections.UserBasicInfo;
import com.yasser.microservice1.domain.search.matcher.UserExampleMatcher;
import com.yasser.microservice1.domain.search.specification.TsSpecification;
import com.yasser.microservice1.domain.search.specification.UserSpecification;
import com.yasser.microservice1.repository.AuthorityRepository;
import com.yasser.microservice1.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;

    public UserService(UserRepository userRepository, AuthorityRepository authorityRepository) {
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
    }

    @Transactional(readOnly = true)
    public List<String> getAuthorities() {
        log.debug("Request to get Authorities");

        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public Optional<User> findById(String id) {
        log.debug("Request to get User By Id");

        return userRepository.findById(id);
    }

    @Caching(evict = {@CacheEvict(value = UserRepository.USERS_BY_LOGIN_CACHE, allEntries = true)})
    public List<User> saveAll(List<User> users) {
        log.debug("Request to save users : {} ", users);

      return userRepository.saveAll(users);
    }

    @Caching(evict = {@CacheEvict(value = UserRepository.USERS_BY_LOGIN_CACHE, allEntries = true)})
    public User save(User user) {
        log.debug("Request to save user : {} ", user);

        return userRepository.save(user);
    }

    public User create(User user) {
        log.debug("Request to create user : {} ", user);

        user.setActive(true);
        return save(user);
    }

    public User update(User user) {
        log.debug("Request to create user : {} ", user);

        return save(user);
    }

    @Transactional(readOnly = true)
    public List<User> queryFirst10ByLastname(String lastname) {
        log.debug("query First 10 By Lastname : {} ", lastname);

        return userRepository.queryFirst10ByLastName(lastname);
    }

    @Transactional(readOnly = true)
    @Cacheable(cacheNames = UserRepository.USERS_BY_EMAIL_CACHE, key = "#email")
    public Optional<User> findByEmailAddress(String email) {
        log.debug("Request to get user by email : {} ", email);

        return userRepository.findByEmailAddress(email);
    }

    @Transactional(readOnly = true)
    @Cacheable(cacheNames = UserRepository.USERS_BY_EMAIL_CACHE, key = "#email")
    public Optional<UserBasicInfo> findByEmail(String email) {
        log.debug("Request to get user by email : {} ", email);

        return userRepository.findByEmail(email);
    }

    @Transactional(readOnly = true)
    @Cacheable(cacheNames = UserRepository.USERS, key = "#id")
    public List<User> findAll(UserSearchDTO userSearchDTO) {

        List<TsSpecification> specifications = UserSpecification.specificationBuilder(userSearchDTO);

        if (specifications != null) {

            return userRepository.findAll(TsSpecification.build(specifications));
        }

        return userRepository.findAll();
    }

    @Transactional(readOnly = true)
    @Cacheable(cacheNames = UserRepository.USERS, key = "#id")
    public List<User> matchingAll(UserSearchDTO userSearchDTO) {

        Example<User> userExample = UserExampleMatcher.matchingAll(userSearchDTO);

        if (userExample != null) {

            return userRepository.findAll(userExample);
        }

        return userRepository.findAll();
    }
}