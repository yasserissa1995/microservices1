package com.yasser.microservice1.service;

import com.yasser.microservice1.domain.model.*;
import com.yasser.microservice1.domain.model.enums.BillStatus;
import com.yasser.microservice1.repository.BillRepository;
import jakarta.persistence.LockModeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class BillService {

    private final Logger log = LoggerFactory.getLogger(BillService.class);
    private final BillRepository billRepository;

    public BillService(BillRepository billRepository) {
        this.billRepository = billRepository;
    }

    @Lock(LockModeType.READ)
    @Transactional(readOnly = true)
    public Optional<? extends Bill> findById(Long id) {
        log.debug("Request to get User By Id");

        return billRepository.findById(id);
    }

    public Bill save(Bill bill) {
        log.debug("start save bill ");

        if (bill instanceof ElectricityBill electricityBill) {
            //Here to add an extra fields from backend
            return billRepository.save(electricityBill);
        }
        if (bill instanceof WaterBill waterBill) {
            //Here to add an extra fields from backend
            return billRepository.save(waterBill);
        }
        if (bill instanceof FuelBill fuelBill) {
            //Here to add an extra fields from backend
            return billRepository.save(fuelBill);
        }
        if (bill instanceof InternetBill internetBill) {
            //Here to add an extra fields from backend
            return billRepository.save(internetBill);
        }
        if (bill instanceof PhoneBill phoneBill) {
            //Here to add an extra fields from backend
            return billRepository.save(phoneBill);
        }

        return billRepository.save(bill);
    }

    public Bill create(Bill bill) {
        log.debug("start create bill ");

        bill.setBillStatus(BillStatus.NEW);
        return save(bill);
    }

    public Bill edit(Bill bill) {
        log.debug("start save bill ");

        return save(bill);
    }
}