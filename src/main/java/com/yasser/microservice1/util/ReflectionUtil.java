package com.yasser.microservice1.util;

import com.yasser.microservice1.domain.model.BaseEntity;
import com.yasser.microservice1.exceptions.type.FormMetaStructureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.*;

public class ReflectionUtil {

	private ReflectionUtil() {
	}

	private static final Logger log = LoggerFactory.getLogger(ReflectionUtil.class);

	public static Class<?> getFieldType(String entity, String field) {

		if (field == null || field.contains(".") || field.contains(";") || field.contains(",") || field.isEmpty()) {
			return null;
		}
		try {
			Class<?> cls = Class.forName(entity);

			Field f = cls.getDeclaredField(field);

			return f.getType();

		} catch (ClassNotFoundException | NoSuchFieldException e) {
			log.debug("", e);
		}

		return null;
	}

	public static Map<String, Field> getClassFields(String entity, int limit) {

		Map<String, String> stackoverflowStopper = new HashMap<>();
		Map<String, Field> pathes = new HashMap<>();

		try {
			Class<?> clazz = Class.forName(entity);

			getClassFields("", "", clazz, null, 1, limit, pathes, stackoverflowStopper);
		} catch (ClassNotFoundException e) {
			log.error("Error in getting fields of entity : {} ", entity);

			throw new FormMetaStructureException("No entity found with name : " + entity);

		}

		return pathes;
	}

	private static void getClassFields(String fullParent, String parent, Class<?> clazz, Field field, int level, int limit,
			Map<String, Field> pathes, Map<String, String> stackoverflowStopper) {

		List<Field> fieldsList = new ArrayList<>();
		level += 1;

		if (Collection.class.isAssignableFrom(clazz) && field != null) {
			ParameterizedType genericType = (ParameterizedType) field.getGenericType();
			Class<?> baseEntityClass = (Class<?>) genericType.getActualTypeArguments()[0];

			if (BaseEntity.class.isAssignableFrom(baseEntityClass)) {
				clazz = baseEntityClass;
			}
		}

		fieldsList = getAllFields(fieldsList, clazz);

		fullParent = fullParent.concat(parent.isEmpty() ? "" : parent + ".");

		for (Field f : fieldsList) {

			if (level <= limit && (BaseEntity.class.isAssignableFrom(f.getType()) || Collection.class.isAssignableFrom(f.getType()))
					&& !stackoverflowStopper.containsKey(parent + "-" + f.getName())) {

				stackoverflowStopper.put(fullParent + "-" + f.getName(), fullParent + "-" + f.getName());

				getClassFields(fullParent, f.getName(), f.getType(), f, level, limit, pathes, stackoverflowStopper);
			}

			pathes.put(fullParent.concat(f.getName()), f);

		}

	}

	private static List<Field> getAllFields(List<Field> fields, Class<?> type) {
		fields.addAll(Arrays.asList(type.getDeclaredFields()));

		if (type.getSuperclass() != null) {
			getAllFields(fields, type.getSuperclass());
		}

		return fields;
	}

	public static Object invokeGetterMethod(Object object, String fieldName) {

		try {
			for (PropertyDescriptor pd : Introspector.getBeanInfo(object.getClass()).getPropertyDescriptors()) {
				if (pd.getName().equals(fieldName) && pd.getReadMethod() != null) {
					return pd.getReadMethod().invoke(object);
				}
			}
		} catch (IntrospectionException | IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Field[] getDeclaredFields(Class<?> clazz, boolean recursively) {
		List<Field> fields = new LinkedList<>();
		Field[] declaredFields = clazz.getDeclaredFields();
		Collections.addAll(fields, declaredFields);

		Class<?> superClass = clazz.getSuperclass();

		if (superClass != null && recursively) {
			Field[] declaredFieldsOfSuper = getDeclaredFields(superClass, recursively);
			if (declaredFieldsOfSuper.length > 0)
				Collections.addAll(fields, declaredFieldsOfSuper);
		}

		return fields.toArray(new Field[fields.size()]);
	}

	public static Field[] getAnnotatedDeclaredFields(Class<?> clazz, Class<? extends Annotation> annotationClass,
			boolean recursively) {
		Field[] allFields = getDeclaredFields(clazz, recursively);
		List<Field> annotatedFields = new LinkedList<>();

		for (Field field : allFields) {
			if (field.isAnnotationPresent(annotationClass))
				annotatedFields.add(field);
		}

		return annotatedFields.toArray(new Field[annotatedFields.size()]);
	}

}
