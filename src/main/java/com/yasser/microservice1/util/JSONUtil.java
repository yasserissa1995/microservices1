package com.yasser.microservice1.util;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.yasser.microservice1.domain.model.TransField;

public class JSONUtil {

    private JSONUtil() {
    }

    public static String convertTransFieldToJson(TransField transField) {
        Gson gson = new Gson();

        return gson.toJson(transField, TransField.class);
    }

    public static TransField convertJsonToTransField(String json) {
        Gson gson = new Gson();
        try {
            return gson.fromJson(json, TransField.class);
        } catch (JsonSyntaxException e) {

            return null;
        }
    }

}
