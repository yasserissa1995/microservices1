package com.yasser.microservice1.util.mapper;

import com.yasser.microservice1.domain.model.User;
import com.yasser.microservice1.domain.model.dto.UserAuditingDTO;
import com.yasser.microservice1.domain.model.dto.UserSearchDTO;

public class UserMapper {

	private UserMapper() {
	}

	public static UserSearchDTO userToUserSearchDTO(User user) {
		return new UserSearchDTO(user);
	}

	public static User userSearchDTOToUser(UserSearchDTO userSearchDTO) {

		if (userSearchDTO == null) {

			return null;

		} else {

			User user = new User();
			user.setLogin(userSearchDTO.getUsername());
			user.setFirstName(userSearchDTO.getFirstName());
			user.setLastName(userSearchDTO.getLastName());
			user.setEmail(userSearchDTO.getEmail());
			return user;
		}
	}

	public static UserAuditingDTO userToUserAuditingDTO(User user) {
		return new UserAuditingDTO(user);
	}

}
