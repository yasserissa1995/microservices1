package com.yasser.microservice1.util;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;

public class DateUtil {

	private DateUtil() {
	}

	public static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

	public static LocalDate parseDate(String date, String format) {

		if (date == null) {
			return null;
		}

		return LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE);

	}

	public static Date getDateFromLocalDateTime(LocalDateTime localDateTime) {
		Instant instant = localDateTime.atZone(ZoneId.systemDefault()).toInstant();

		return Date.from(instant);
	}

	public static Date getDateFromLocalDate(LocalDate localDate) {

		return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	public static LocalDateTime parseDatetime(String date, String format) {

		if (date == null) {
			return null;
		}

		return LocalDateTime.parse(date, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
	}

	public static String fromat(TemporalAccessor date) {
		if (date == null) {
			return null;

		}

		return DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT).format(date);
	}

	public static String fromat(TemporalAccessor date, String format) {
		if (date == null) {
			return null;

		}

		if (format.isEmpty()) {
			format = DEFAULT_DATE_FORMAT;
		}

		return DateTimeFormatter.ofPattern(format).format(date);
	}

	public static String fromat(Date date) {
		if (date == null) {
			return null;

		}

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);

		return simpleDateFormat.format(date);
	}

	public static String fromat(Date date, String format) {

		if (date == null) {
			return null;
		}

		if (format.isEmpty()) {
			format = DEFAULT_DATE_FORMAT;
		}

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);

		return simpleDateFormat.format(date);
	}

	public static String fromatDate(String date, String format) {

		if (date == null) {
			return null;

		} else if ("".equals(date)) {
			return "";
		}

		if (format.isEmpty()) {
			format = DEFAULT_DATE_FORMAT;
		}

		return DateTimeFormatter.ofPattern(format).format(parseDate(date, format));
	}

	public static String fromatDatetime(String date, String format) {
		if (date == null) {
			return null;

		} else if ("".equals(date)) {
			return "";
		}

		if (format.isEmpty()) {
			format = DEFAULT_DATE_TIME_FORMAT;
		}

		return DateTimeFormatter.ofPattern(format).format(parseDatetime(date, format));

	}

	public static LocalDateTime getLocalDateTimeFromDate(Date date) {
		Instant current = date.toInstant();

		return LocalDateTime.ofInstant(current, ZoneId.systemDefault());
	}
}