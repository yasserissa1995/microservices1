package com.yasser.microservice1;

import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import java.util.Arrays;

@SpringBootApplication
public class Microservice1Application {

	private static final Logger log = LoggerFactory.getLogger(Microservice1Application.class);

	private final Environment env;

	public Microservice1Application(Environment env) {
		this.env = env;
	}

	public static void main(String[] args) {
		SpringApplication.run(Microservice1Application.class, args);
	}

	@PostConstruct
	public void initApplication() {

		log.debug("Active profiles are  {}:", Arrays.asList(env.getActiveProfiles()));
	}

}
